import subprocess
from Screen import screen

class ScreenManager:
    def __init__(self):
        self.xrandr = self.getXrandr()
        self.screensDict, self.length = self.getScreenDictionary(self.xrandr)
        self.screens = {}

    def getXrandr(self):
        ps = subprocess.Popen("xrandr", stdout=subprocess.PIPE)
        xrandr = (ps.communicate()[0].decode('UTF-8')).split("\n")
        return xrandr

    def getScreenDictionary(self, xrandr):
        screens = {}
        for element in xrandr:
            screenColumn = self.getScreenNameColumn(element)
            if screenColumn != '' and screenColumn != "Screen":
                index = xrandr.index(element)
                screens[screenColumn] = index
        length = len(xrandr)
        return screens, length

    def getScreenNameColumn(self, element):
        screenNameColumn = element.split('\n')[0].split(" ")[0].split(" ")[0]
        return screenNameColumn

    def createScreens(self):
        keys = []
        end = 0
        for key, value in self.screensDict.items():
            keys.append(key)
        i = 0
        for key, value in self.screensDict.items():
            self.screens[key] = screen.Screen(key)
            try:
                end = self.screensDict[keys[i+1]]
            except:
                end = self.length
            self.screens[key].getResolutions(self.xrandr, value, end)
            i += 1

