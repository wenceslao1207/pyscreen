import subprocess
import time
import curses
from ScreenManager import screenManager

POSITIONS=["above", "below", "rigth-of", "left-of", "same-as"]
screen = screenManager.ScreenManager()
screen.createScreens()
menu = []
for key, value in screen.screensDict.items():
    menu.append(key)
menu.append('Exit')
resolutionsMenu = []

def setResolutionMenu(screenName):
    resolutions = screen.screens[screenName].availResolution
    return resolutions


def setResolution(selectedScreen, selectedResolution):
    resolution = selectedResolution
    selectedScreen.setResolution(resolution)
    configureScreen(selectedScreen)


def configureScreen(screenSelected):
    screenSelected.configure()

def printPositionsMenu(stdscr):
    #stdscr.clear()
    selected_row_idx = 0
    h, w = stdscr.getmaxyx()
    for idx, row in enumerate(POSITIONS):
        x = (w//5)*4 - len(row)//2
        y = h//2 - len(POSITIONS)//2 + idx

        if idx == selected_row_idx:
            stdscr.attron(curses.color_pair(1))
            stdscr.addstr(y, x, row)
            stdscr.attroff(curses.color_pair(1))
        else:
            stdscr.attron(curses.color_pair(2))
            stdscr.addstr(y, x, row)
            stdscr.attroff(curses.color_pair(2))
    stdscr.refresh()

def printMainMenu(stdscr, selected_row_idx):
    stdscr.clear()
    h, w = stdscr.getmaxyx()
    stdscr.attron(curses.color_pair(2))
    x = w//5 - 1
    y = h//2 - len(menu)-2
    stdscr.addstr(y, x, "-------------")
    y += 1
    stdscr.addstr(y, x, "Select Screen")
    y += 1
    stdscr.addstr(y, x, "-------------")

    for idx, row in enumerate(menu):
        x = w//5 - 0
        y = h//2 - len(menu)//2 + idx

        if idx == selected_row_idx:
            stdscr.attron(curses.color_pair(1))
            stdscr.addstr(y, x, row)
            stdscr.attroff(curses.color_pair(1))
        else:
            stdscr.attron(curses.color_pair(2))
            stdscr.addstr(y, x, row)
            stdscr.attroff(curses.color_pair(2))
    stdscr.refresh()


def printScreenMenu(stdscr, selected_row_idx, resolutionsMenu):
    h, w = stdscr.getmaxyx()
    for idx, row in enumerate(resolutionsMenu):
        x = w//2
        y = h//2 - len(resolutionsMenu)//2 + idx

        if idx == selected_row_idx:
            stdscr.attron(curses.color_pair(1))
            stdscr.addstr(y, x, row)
            stdscr.attroff(curses.color_pair(1))
        else:
            stdscr.attron(curses.color_pair(2))
            stdscr.addstr(y, x, row)
            stdscr.attroff(curses.color_pair(2))
    stdscr.refresh()


def printResolutionMenu(stdscr, screenSelected, resolutionsMenu):
    current_row = 0
    printScreenMenu(stdscr, current_row, resolutionsMenu)
    while 1:
        key = stdscr.getch()
        if key == curses.KEY_UP and current_row > 0:
            current_row -= 1
        elif key == curses.KEY_DOWN and current_row < len(resolutionsMenu)-1:
            current_row += 1
        elif key == curses.KEY_ENTER or key in [10, 13]:
            if current_row == len(resolutionsMenu)-1:
                break
            selectedScreen = screen.screens[screenSelected]
            setResolution(selectedScreen, resolutionsMenu[current_row])
        printScreenMenu(stdscr, current_row, resolutionsMenu)


def printCenter(stdscr, text):
    stdscr.clear()
    h, w = stdscr.getmaxyx()
    x = w//2 - len(text)//2
    y = h//2
    stdscr.addstr(y, x, text)
    stdscr.refresh()

def main(stdscr):

    curses.curs_set(0)
    if curses.can_change_color():
        curses.init_pair(0,0,0,0)
    curses.init_pair(1, curses.COLOR_BLACK, curses.COLOR_WHITE)
    curses.init_pair(2 , curses.COLOR_WHITE, curses.COLOR_BLACK)
    current_row = 0
    printMainMenu(stdscr, current_row)
    stdscr.attron(curses.color_pair(2))
    selected = 0

    while 1:
        key = stdscr.getch()

        if key == curses.KEY_UP and current_row > 0:
            current_row -= 1
        elif key == curses.KEY_DOWN and current_row < len(menu)-1:
            current_row += 1
        elif key == curses.KEY_ENTER or key in [10, 13]:
            selected = current_row
            if current_row == len(menu)-1:
                break
            current_row = 0
            resolutionsMenu = setResolutionMenu(menu[current_row])
            resolutionsMenu.append("BACK")
            printResolutionMenu(stdscr, menu[selected], resolutionsMenu)
            del resolutionsMenu[-1]
        elif key == 27:
            break
        printMainMenu(stdscr, current_row)


curses.wrapper(main)
