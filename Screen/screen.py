import subprocess

class Screen:
    def __init__(self, name):
        self.name = name
        self.resolution = None
        self.position = None
        self.positions = ["above", "below", "rigth-to", "left-to", "same-as"]
        self.availResolution = None

    def getResolutions(self, xrandr, start, end):
        resolutions = []
        for i in range (start, end):
            element = xrandr[i].split('\n')[0].split(" ")
            try:
                resolutions.append(element[3])
            except:
                pass
        resolutions.pop(0)
        self.availResolution = resolutions
        pass

    def setPosition(self, position):
        self.position = position
        return self

    def setResolution(self, resolution):
        self.resolution = resolution
        return self

    def configure(self):
        subprocess.call(["xrandr", "--output", self.name,
                         "--mode",self.resolution])
